// function cacheFunction(cb) {
//     // Should return a function that invokes `cb`.
//     // A cache (object) should be kept in closure scope.
//     // The cache should keep track of all arguments have been used to invoke this function.
//     // If the returned function is invoked with arguments that it has already seen
//     // then it should return the cached result and not invoke `cb` again.
//     // `cb` should only ever be invoked once for a given set of arguments.
// }

const cacheFunction = (cb) => {
    if (typeof (cb) !== 'function') {
        throw new Error('Invalid Function');
    }
    let cache = {};
    const returnFunction = (...args) => {
        if (args.length === 0) {
            return cb();
        }
        const key = JSON.stringify(args);
        if (key in cache) {
            console.log(`used for ${cache[key]}`)
            return cache[key];
        }
        let result = cb(...args);
        cache[key] = result;
        return result;
    }
    return returnFunction;
}

module.exports = cacheFunction;