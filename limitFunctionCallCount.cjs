// function limitFunctionCallCount(cb, n) {
//     // Should return a function that invokes `cb`.
//     // The returned function should only allow `cb` to be invoked `n` times.
//     // Returning null is acceptable if cb can't be returned
// }

const limitFunctionCallCount = (cb, n) => {
    let count = 0;
    if (typeof (cb) !== 'function' || (typeof (n) !== 'number' || n < 1)) {
        throw new Error('Invalid Parameters. Provide a callback function');
    }
    const returnFunction = (...args) => {
        if (count++ < n) {
            return cb(...args)
        } else {
            return null;
        }
    }
    return returnFunction;
}

module.exports = limitFunctionCallCount;