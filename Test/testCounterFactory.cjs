const counterFactory = require('../counterFactory.cjs');

const result = counterFactory();

console.log(result.increment());
console.log(result.increment());
console.log(result.decrement());
console.log(result.decrement());