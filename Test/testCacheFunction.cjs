const cacheFunction = require('../cacheFunction.cjs');

const cb = (num) => {
    return num;
};

const returnFunction = cacheFunction(cb);

console.log(returnFunction(5));
console.log(returnFunction(5));

console.log(returnFunction(7));
console.log(returnFunction(7));
console.log(returnFunction(7));
console.log(returnFunction(7));
console.log(returnFunction(7));
console.log(returnFunction(7));
console.log(returnFunction(7));
console.log(returnFunction(7));
