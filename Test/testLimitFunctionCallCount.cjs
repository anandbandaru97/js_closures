const limitFunctionCallCount = require('../limitFunctionCallCount.cjs');

const cb = (n) => {
    return `cb function invoked times: ${n}`;
};

const returnFunction = limitFunctionCallCount(cb, 10);

if (typeof returnFunction !== 'function') {
    console.log(returnFunction);
} else {
    for (let index = 1; index <= 15; index++) {
        console.log(returnFunction(index));
    }
}